// Import Queries
const user = require('./Query/user')
const project = require('./Query/project')
const form = require('./Query/form')


const Query = {
  test: () => true,
  ...user,
  ...form,
  ...project
}

module.exports = { Query }
