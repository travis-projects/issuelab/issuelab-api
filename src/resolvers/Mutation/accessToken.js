const { getUserId } = require('../../utils')
const axios = require('axios')

const accessToken = {
  async createAccessToken (parent, args, ctx, info) {
    // Auth check
    const userId = getUserId(ctx)
    if (!userId) throw new Error(`Not authorized!`)
    // Select redirect URI
    const redirect_uri = process.env.GITLAB_REDIRECT_URI || 'http://localhost:8080/authenticate/redirect'
    // make request to get access token
    const { data: token } = await axios({
      method: 'post',
      url: 'https://gitlab.com/oauth/token',
      params: {
        client_id: process.env.GITLAB_APP_ID,
        client_secret: process.env.GITLAB_SECRET,
        code: args.code,
        redirect_uri,
        grant_type: 'authorization_code'
      }
    })
    const { data: { id: gitlabId }} = await axios({
      method: 'get',
      url: 'https://gitlab.com/api/v4/user',
      params: {
        access_token: token.access_token
      }
    })
    return await ctx.db.mutation.updateUser({
      where: { id: userId },
      data: {
        gitlabId,
        token,
        authenticated: true
      }
    }, info).then(() => ({ response: 200 })).catch(err => {
      console.error(err)
      return { response: 500 }
    })
  }
}

module.exports = accessToken