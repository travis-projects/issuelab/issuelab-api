const bcrypt = require('bcryptjs')
const upash = require('upash')
const jwt = require('jsonwebtoken')

upash.install('argon2', require('@phc/argon2'))

const auth = {
  async signup(parent, { auth }, ctx, info) {
    const password = await upash.hash(auth.password)
    const { email, name } = auth
    const user = await ctx.db.mutation.createUser({
      data: { email, name , password }
    })
    return {
      token: jwt.sign({ userId: user.id }, process.env.APP_SECRET),
      user
    }
  },

  async login(parent, { auth: { email, password } }, ctx, info) {
    const user = await ctx.db.query.user({ where: { email } })
    if (!user) {
      throw new Error(`No such user found for email: ${email}`)
    }

    const valid = await upash.verify(user.password, password)
    if (!valid) {
      throw new Error('Invalid password')
    }

    return {
      token: jwt.sign({ userId: user.id }, process.env.APP_SECRET),
      user,
    }
  },
}

module.exports = { auth }
