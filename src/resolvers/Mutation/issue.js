const { getUserId } = require('../../utils')
const axios =require('axios')

const issue = {
  async createIssue (parent, { userId, issue }, ctx, info) {
    // Get Access Token
    const { token: { access_token } } = await ctx.db.query.user({
      where: { id: userId }
    })
    // Create Issue
    return axios({
      method: 'post',
      url: `https://gitlab.com/api/v4/projects/${issue.projectId}/issues`,
      params: {
        access_token,
        id: issue.projectId,
        title: issue.name,
        description: issue.description,
        labels: `Issue Lab, Client Feedback`
      }
    }).then(({ data }) => {
      return { iid: data.iid, web_url: data.web_url }
    }).catch(err => {
      throw new Error(err)
    })
  }
}

module.exports = issue
