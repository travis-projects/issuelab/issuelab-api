const { getUserId } = require('../../utils')

const form = {
  async createForm (parent, { form }, ctx, info) {
    const userId = getUserId(ctx)
    if (!userId) throw new Error(`Not Authorized`)
    // Create form in DB
    return ctx.db.mutation.createForm({
      data: {
        user: {
          connect: { id: userId }
        },
        ...form
      }
    })
  },
  async deleteForm (parent, { id }, ctx, info) {
    const userId = getUserId(ctx)
    if (!userId) throw new Error(`Not Authorized`)
    // Create form in DB
    return ctx.db.mutation.deleteForm({
      where: { id }
    })
  },
  async updateForm (parent, { id, form }, ctx, info) {
    const userId = getUserId(ctx)
    if (!userId) throw new Error(`Not Authorized`)
    // Create form in DB
    return ctx.db.mutation.updateForm({
      where: { id },
      data: { ...form }
    })
  }
}

module.exports = form
