const { getUserId } = require('../../utils')
const axios = require('axios')

const form = {
  form (parent, args, ctx, info) {
    return ctx.db.query.form({ where: { id: args.id } }, info)
  },
  allForms (parent, args, ctx, info) {
    const id = getUserId(ctx)
    if (!id) throw new Error(`Not Authorized, no user`)
    return ctx.db.query.forms({
      where: { user: { id } }
    }, info)
  }
}

module.exports = form
