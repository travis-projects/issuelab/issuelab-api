const { getUserId } = require('../../utils')
const axios = require('axios')

const user = {
  me (parent, args, ctx, info) {
    const id = getUserId(ctx)
    if (!id) throw new Error(`Not Authorized, no user`)
    return ctx.db.query.user({ where: { id } }, info)
  }
}

module.exports = user