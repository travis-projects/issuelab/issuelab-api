const { getUserId } = require('../../utils')
const axios = require('axios')

const project = {
  async allProjects (parent, args, ctx, info) {
    const userId = getUserId(ctx)
    if (!userId) throw new Error(`Not Authorized`)
    // Gitlab access data 
    const { token: { access_token } } = await ctx.db.query.user({
      where: { id: userId }
    })
    // Start getting projects
    const { data: projects } = await axios({
      method: 'get',
      url: `https://gitlab.com/api/v4/projects`,
      params: { access_token, order_by: 'updated_at', simple: true, membership: true, min_access_level: 30, per_page: 100 }
    })
    console.log(projects)
    return projects
  } /*,
  async userGroups (parent, args, ctx, info) {
    const userId = getUserId(ctx)
    if (!userId) throw new Error(`Not Authorized`)
    const { token: { access_token } } = await ctx.db.query.user({
      where: { id: userId }
    })
    return axios({
      method: 'get',
      url: `https://gitlab.com/api/v4/groups`,
      params: { access_token }
    }).then(({ data }) => ({ response: 200, body: data}))
  },
  async groupProjects (parent, args, ctx, info) {
    const userId = getUserId(ctx)
    if (!userId) throw new Error(`Not Authorized`)
    const { token: { access_token } } = await ctx.db.query.user({
      where: { id: userId }
    })
    return axios({
      method: 'get',
      url: `https://gitlab.com/api/v4/groups/${args.groupId}/projects`,
      params: { access_token, order_by: 'updated_at' }
    }).then(({ data }) => ({ response: 200, body: data}))
  } */
}

module.exports = project
