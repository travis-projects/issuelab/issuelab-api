const { Query } = require('./Query')
// const { Subscription } = require('./Subscription')
const { auth } = require('./Mutation/auth')
// const { post } = require('./Mutation/post')
const accessToken = require('./Mutation/accessToken')
const form = require('./Mutation/form')
const issue = require('./Mutation/issue')
const { AuthPayload } = require('./AuthPayload')

module.exports = {
  Query,
  Mutation: {
    ...auth,
    ...accessToken,
    ...form,
    ...issue
  },
  AuthPayload,
}
